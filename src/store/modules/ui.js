const defaultState = () => ({
  loading: false
})

export const ui = {
  namespaced: true,
  state: defaultState(),

  mutations: {
    /**
     * Set loading state
     * @param state
     * @param payload
     */
    setLoading (state, payload) {
      state.loading = payload
    }
  },

  getters: {
    /**
     * Return status
     * @param state
     */
    getStatus: state => state.loading
  }
}
