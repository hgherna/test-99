export const COMPANIES_INDEX = () => 'https://dev.ninetynine.com/testapi/1/companies'
export const COMPANIES_DETAILS = (company) => `https://dev.ninetynine.com/testapi/1/companies/${company}`
