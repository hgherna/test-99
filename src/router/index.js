import Vue from 'vue'
import Router from 'vue-router'
import Searcher from '../containers/searcher/Searcher.vue'
import Details from '../containers/details/Details.vue'
import Errors from '../containers/errors/Errors'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'searcher',
      component: Searcher
    },
    {
      path: '/details/:company',
      name: 'details',
      component: Details
    },
    {
      path: '*',
      name: 'errors',
      component: Errors
    }
  ]
})
