import { http } from './index'
import axios from 'axios'
import { COMPANIES_INDEX, COMPANIES_DETAILS } from '../../helpers/constrains'

/**
 * This cancel token lets you cancel request on demand
 * @type {CancelTokenStatic|*}
 */
const CancelToken = axios.CancelToken
let cancelations = []

class CompaniesInfo {
  /**
   * Request all companies
   */
  static requestCompanies = () => {
    return http.get(COMPANIES_INDEX(),
      {
        cancelToken: new CancelToken(function executor (c) {
          cancelations.push({ token: c, service: 'requestCompanies' })
        })
      }
    )
  }

  /**
   * Request details for company
   * @param company
   * @returns {*}
   */
  static requestCompaniesDetails = (company) => {
    return http.get(COMPANIES_DETAILS(company),
      {
        cancelToken: new CancelToken(function executor (c) {
          cancelations.push({ token: c, service: 'requestCompaniesDetails' })
        })
      }
    )
  }
}

export { CompaniesInfo, cancelations }
