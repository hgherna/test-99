import { shallowMount, RouterLinkStub } from '@vue/test-utils'
import Card from '../../src/components/card/Card'

describe('---- CardComponent ----', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Card, {
      propsData: {
        logo: '',
        name: 'Apple Inc.',
        description: 'Lorem ipsum is your friend ;)',
        price: 229.324,
        company: 1
      },
      stubs: {
        RouterLink: RouterLinkStub
      }
    })
  })

  describe('Mount and match snapshot', () => {
    it('Renders correctly', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('Render correct props', () => {
    it('Render props correctly', () => {
      const nameText = wrapper.findAll('.card__header__name').at(0)
      const priceText = wrapper.findAll('.card__price').at(0)
      const routerLink = wrapper.find(RouterLinkStub)

      expect(nameText.text()).toBe('Apple Inc.')
      expect(priceText.text()).toBe('229.324 €')
      expect(routerLink.props().to).toEqual({ 'name': 'details', 'params': { 'company': 1 } })
    })
  })
})
